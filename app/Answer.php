<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $table = 'answer';

    protected $fillable = [
        'id',
        'value',
        'status',
        'has_comment',
        'question_id'
    ];

    protected $guarded = [];

    public function question() {
        return $this->belongsTo(Question::class,'question_id');
    }

    public function results() {
        return $this->hasMany(Result::class);
    }
    
}
