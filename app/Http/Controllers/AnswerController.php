<?php

namespace App\Http\Controllers;

use App\Answer;
use App\Question;
use App\Survey;
use Illuminate\Http\Request;

class AnswerController extends Controller
{

    public function create($id)
    {   
        $question = Question::findOrFail($id);
        //dd($question->type);
        $survey = $question->survey;
        $answers = $question->answers;
        //dd($answers);

                
        return view('answer.create', array(
            'question' => $question,
            'answers' => $answers,
            'survey' => $survey
        ));
        
    }

    public function store(Request $request)
    {
        $question_id = $request->question_id;
        $answer = Answer::create($request->all());
        if($answer) {
            return redirect("/answers/create/$question_id")->with('success','Answer created successfully');
        }
        
    }

    public function edit($id)
    {
        $answer = Answer::findOrFail($id);
        return view('answer.edit', [
            'answer' => $answer
        ]);
    }

    public function update($id, Request $request)
    {   
        $question_id = $request->question_id;
        $answer = Answer::findOrFail($id);
        $answer->value = $request->value;
        $answer->status = $request->status;
        $answer->has_comment = $request->has_comment;
        $answer->save();

        if ($answer) {
            return redirect("/answers/create/$question_id")->with('success','Answer updated successfully');
        }
    }

    public function destroy($id, Request $request)  
    {
        $question_id = $request->question_id;
        $answer = Answer::destroy($id);
        if ($answer) {
            return redirect("/answers/create/$question_id");
        }
    }
}
