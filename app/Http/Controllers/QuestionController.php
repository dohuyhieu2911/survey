<?php

namespace App\Http\Controllers;

use App\Question;
use App\Survey;
use Illuminate\Http\Request;

class QuestionController extends Controller
{
    public function index($id)
    {
        $survey = Survey::findOrFail($id);
        $questions = $survey->questions;

        return view('Question.index', [
            'questions' => $questions,
            'survey' => $survey,
        ]);
    }

    public function create($id)
    {
        $survey = Survey::findOrFail($id);
        return view('Question.create',[
            'survey' => $survey
        ]);
    }

    public function store(Request $request)
    {
        $survey_id = $request->survey_id;
        $question = Question::create($request->all());
        if($question) {
            return redirect("/question/$survey_id")->with('success','Question created successfully');
        }
        
    }

    public function edit($id)
    {
        $question = Question::findOrFail($id);
        return view('Question.edit', [
            'question' => $question
        ]);
    }

    public function update($id, Request $request)
    {
        $survey_id = $request->survey_id;
        $question = Question::findOrFail($id);
        $question->name = $request->name;
        $question->type = $request->type;
        $question->status = $request->status;
        $question->sort = $request->sort;
        
        $question->save();
        if ($question) {
            return redirect("/question/$survey_id")->with('success','Question updated successfully');
        }
    }

    public function destroy($id, Request $request)
    {   
        $survey_id = $request->survey_id;
        $question = Question::destroy($id);
        if ($question) {
            return redirect("/question/$survey_id");
        }
    }
}
