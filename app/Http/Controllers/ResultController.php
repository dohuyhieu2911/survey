<?php

namespace App\Http\Controllers;

use App\Answer;
use App\Result;
use App\Survey;
use Illuminate\Http\Request;

class ResultController extends Controller
{
    //Get all data in result
    public function index()
    {
        $results = Result::all();
        return view('result.index', [
            'results' => $results
        ]);
    }


    // Process data from user
    public function store(Request $request)
    {     
        $data = $request->all(); 
        //dd($data);
        $survey_id = $request->survey_id; 
        unset($data['survey_id']);
        
        foreach ($data as $key => $val)
        {   
            if(!is_array($val)) {
                $result = new Result;
                $result->survey_id = $survey_id;
                $result->question_id = $key;
                $result->comment = $val;
                $result->save();
            }else {
                if($val['type'] == "radio"){
                    //dd($val['value']);
                    $result = new Result;
                    $result->survey_id = $survey_id;
                    $result->question_id = $key;
                    $result->answer_id =  $val['value'];
                    foreach($val['comment'] as $key1 => $val1) {
                        if($val1 != NULL) {
                            $result->comment = $val1;
                        }                        
                    }
                    $result->save();                       
                    
                }elseif($val['type'] == 'checkbox'){ 
                    //dd($val['value']);                   
                    foreach($val['value'] as $key1 => $val1){
                        $result = new Result;
                        $result->survey_id = $survey_id;
                        $result->question_id = $key;
                        $ans_id = strtok($val1, "**");
                        $result->answer_id = $ans_id;
                        $cmt = strtok('**');
                        $result->comment = $cmt;
                        $result->save();                    
                    }                 
                }
            }
        }
        return 1;
    }

}

