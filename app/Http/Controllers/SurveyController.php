<?php

namespace App\Http\Controllers;

use App\Survey;
use Illuminate\Http\Request;

class SurveyController extends Controller
{
    public function index()
    {
        $surveys = Survey::all();
        return view('survey.index', [
            'surveys' => $surveys
        ]);
    }

    public function create()
    {
        return view('survey.create');
    }

    public function store(Request $request)
    {
        $survey = Survey::create($request->all());
        if($survey) {
            return redirect('/surveys')->with('success','Survey created successfully');
        } 
    }

    public function edit($id)
    {
        $survey = Survey::findOrFail($id);
        return view('survey.edit', [
            'survey' => $survey
        ]);
    }

    public function update($id, Request $request)
    {
        $survey = Survey::findOrFail($id)->update($request->input());
        if ($survey) {
            return redirect('/surveys')->with('success','Survey updated successfully');
        }
    }

    public function destroy($id)
    {
        $survey = Survey::destroy($id);
        if ($survey) {
            return redirect('/surveys');
        }
    }
}
