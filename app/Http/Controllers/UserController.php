<?php

namespace App\Http\Controllers;

use App\Answer;
use App\Question;
use App\Survey;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index()
    {
        $surveys = Survey::where('status','=' ,1)->get();
        return view('front.user', [
            'surveys' => $surveys
        ]);
    }

    public function chooseSurvey(int $id)
    {
        $survey = Survey::findOrFail($id);    
        $questions = $survey->questions->where('status', 1)->sortBy('sort');
        return view('front.survey', array(
            'questions' => $questions,
            'survey' => $survey,
        ));
    }

  

   
}
