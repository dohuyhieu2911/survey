<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $table = 'question';

    protected $fillable = [
        'id',
        'survey_id',
        'name',
        'type',
        'status',
        'sort'
    ];

    const TEXT = 1;
    const EMAIL = 2;
    const RADIO_BUTTON = 3;
    const CHECKBOX = 4;
    const RATING = 5;
    

    protected $guarded = [];

    public function survey() {
        return $this->belongsTo(Survey::class,'survey_id');
    }

    public function answers() {
        return $this->hasMany(Answer::class);
    }

    public function results() {
        return $this->hasMany(Result::class);
    }
}
