<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
    protected $table = 'result';

    protected $fillable = [
        'id',
        'survey_id',
        'question_id',
        'answer_id',
        'comment'
    ];

    
    protected $guarded = [];

    public function survey() {
        return $this->belongsTo(Survey::class,'survey_id');
    }

    public function question() {
        return $this->belongsTo(Question::class,'question_id');
    }

    public function answer() {
        return $this->belongsTo(Answer::class,'answer_id');
    }
    
}
