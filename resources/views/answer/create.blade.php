@extends('layouts.app-back')

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Answer Tables</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/admin') }}">Home</a>
            </li>
            <li>
                <a>Tables</a>
            </li>
            <li>
                <a href="{{ url('/surveys') }}">Surveys Tables</a>
            </li>
            <li>
                <a href="{{ url("/question/$survey->id") }}">{{ $survey->name }}</a>
            </li>
            <li class="active">
                <strong>{{ $question->name }}</strong>
            </li>
        </ol>
    </div>
    
</div>
       
    @if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
    @endif

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Answer Tables example with responsive plugin</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li><a href="#">Config option 1</a>
                                </li>
                                <li><a href="#">Config option 2</a>
                                </li>
                            </ul>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">

                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-example" id="answer_table" aria-describedby="DataTables_Table_0_info" role="grid">
                                @if($answers->isNotEmpty())
                                <thead>
                                    <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Question_id</th>
                                    <th scope="col">Value</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Has_comment</th>
                                    <th scope="col">Created-at</th>
                                    <th scope="col">Updated-at</th>
                                    <th scope="col"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach ($answers as $answer)
                                    <tr>
                                        <th scope="col">{{ $answer->id }}</th>
                                        <td>{{ $answer->question_id }}</td>
                                        <td>{{ $answer->value }}</td>
                                        <td>{{ $answer->status }}</td>
                                        <td>{{ $answer->has_comment }}</td>
                                        <td>{{ $answer->created_at }}</td> 
                                        <td>{{ $answer->updated_at }}</td>
                                        <td>
                                            <a class="btn btn-warning" style="margin-right: 8px" href='{{ url("/answers/$answer->id") }}'>Edit</a>
                                            <a class="btn btn-danger" href="javascript:void(0)" onclick="return confirm('Are you sure delete this answer?') ? document.getElementById('answer-delete-{{ $answer->id }}').submit() : false">Delete</a>
                                            <form action='{{ url("/answers/$answer->id") }}' method="POST" id="answer-delete-{{ $answer->id }}">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <input type="hidden" name="question_id" value="{{ $answer->question_id }}">
                                            </form>
                            
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                @else
                                <div>
                                    <div class="alert alert-warning" role="alert">
                                        No Answers Found!
                                    </div>
                                </div>
                            @endif
                                
                            </table>

                            <div class="ibox">
                                <div class="ibox-title">
                                    <h5>Answers Create</h5>
                                    <div class="ibox-tools">
                                        <a class="collapse-link">
                                            <i class="fa fa-chevron-up"></i>
                                        </a>
                                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                            <i class="fa fa-wrench"></i>
                                        </a>
                                        <ul class="dropdown-menu dropdown-user">
                                            <li><a href="#" class="dropdown-item">Config option 1</a>
                                            </li>
                                            <li><a href="#" class="dropdown-item">Config option 2</a>
                                            </li>
                                        </ul>
                                        <a class="close-link">
                                            <i class="fa fa-times"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="ibox-content">
                                    <form  method="POST" action='{{ route("answers.create") }}'>
                                        {{ csrf_field() }}
                                        <input type="hidden" name="question_id" value="{{ $question->id }}" />
                                    
                                        <div class="form-group row">
                                            <label class="col-lg-2 col-form-label">Value</label>
                                            <div class="col-lg-10">
                                                <input type="text" class="form-control" id="" name="value">
                                            </div>
                                        </div>
                                        <div class="hr-line-dashed"></div>
                                        <div class="form-group row">
                                            <button type="submit" class="btn btn-primary">Submit</button>    
                                        </div> 
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('custom_js')
    <script>
        $(document).ready(function() {
            $('#answer_table').DataTable();
        });
    </script>
@endsection
