@extends('layouts.app-front')
@section('custom_css')
    <style>
        #box-survey-hide {
            position: fixed;
            min-height: 300px;
            width: 280px;
            bottom: 0;
            right: 2rem;
            padding: 2rem;
            background: #263345;
            color: #fff;
            border-radius: 10px 10px 0 0;
        }

        #close {
            position: absolute;
            bottom: 100%;
            text-align: center;
            right: 5%;
            width: 40px;
            line-height: 30px;
            height: 30px;
            background: #263345;
            color: #fff;
            cursor: pointer;
        }

        #box-survey-show {
            position: fixed;
            bottom: -100%;
            right: 2rem;
            background: #263345;
            height: 20px;
            width: 280px;
            border-radius: 10px 10px 0 0;
        }

        #show {
            position: absolute;
            bottom: 100%;
            text-align: center;
            right: 5%;
            width: 40px;
            height: 30px;
            background: #263345;
            color: #fff;
            cursor: pointer;
        }

        .rating-star {
            font-size: 35px;
            margin-left: 50px;
        }

        fieldset {
            display: none;
            margin-top: 15px
        }

        .form-group {
            margin: auto;
            text-align: center;
        }

        label {
            font-size: 13px;
        }

        label:hover {
            opacity: 0.8;
            cursor: pointer;
        }

        .control-label {
            margin-bottom: 10px;
            font-size: 16px;
            color: lightgrey;
        }

        .control-label:hover {
            cursor: context-menu;
        }

        .radio_ans {
            text-align: left;
            margin-bottom: 5px;
        }

        .rating-star span.star:before {
            margin-right: 5px;
        }

        .error {
            color: red;
            text-align: center;
            margin: 10px;
            font-size: 15px
        }

        #thank {
            display: none;
            justify-content: center;
            margin: 100px 50px 100px 50px;
            font-size: 27px;
        }

        .comment {
            display: none;
        }

        .active {
            display: block;
        }

        .pyro>.before,
        .pyro>.after {
            position: absolute;
            width: 7px;
            height: 7px;
            pointer-events: none;
            z-index: 99999999;
            border-radius: 50%;
            box-shadow: -120px -218.66667px blue, 248px -16.66667px #00ff84, 190px 16.33333px #002bff, -113px -308.66667px #ff009d, -109px -287.66667px #ffb300, -50px -313.66667px #ff006e, 226px -31.66667px #ff4000, 180px -351.66667px #ff00d0, -12px -338.66667px #00f6ff, 220px -388.66667px #99ff00, -69px -27.66667px #ff0400, -111px -339.66667px #6200ff, 155px -237.66667px #00ddff, -152px -380.66667px #00ffd0, -50px -37.66667px #00ffdd, -95px -175.66667px #a6ff00, -88px 10.33333px #0d00ff, 112px -309.66667px #005eff, 69px -415.66667px #ff00a6, 168px -100.66667px #ff004c, -244px 24.33333px #ff6600, 97px -325.66667px #ff0066, -211px -182.66667px #00ffa2, 236px -126.66667px #b700ff, 140px -196.66667px #9000ff, 125px -175.66667px #00bbff, 118px -381.66667px #ff002f, 144px -111.66667px #ffae00, 36px -78.66667px #f600ff, -63px -196.66667px #c800ff, -218px -227.66667px #d4ff00, -134px -377.66667px #ea00ff, -36px -412.66667px #ff00d4, 209px -106.66667px #00fff2, 91px -278.66667px #000dff, -22px -191.66667px #9dff00, 139px -392.66667px #a6ff00, 56px -2.66667px #0099ff, -156px -276.66667px #ea00ff, -163px -233.66667px #00fffb, -238px -346.66667px #00ff73, 62px -363.66667px #0088ff, 244px -170.66667px #0062ff, 224px -142.66667px #b300ff, 141px -208.66667px #9000ff, 211px -285.66667px #ff6600, 181px -128.66667px #1e00ff, 90px -123.66667px #c800ff, 189px 70.33333px #00ffc8, -18px -383.66667px #00ff33, 100px -6.66667px #ff008c;
            -moz-animation: 1s bang ease-out infinite backwards, 1s gravity ease-in infinite backwards, 5s position linear infinite backwards;
            -webkit-animation: 1s bang ease-out infinite backwards, 1s gravity ease-in infinite backwards, 5s position linear infinite backwards;
            -o-animation: 1s bang ease-out infinite backwards, 1s gravity ease-in infinite backwards, 5s position linear infinite backwards;
            -ms-animation: 1s bang ease-out infinite backwards, 1s gravity ease-in infinite backwards, 5s position linear infinite backwards;
            animation: 1s bang ease-out infinite backwards, 1s gravity ease-in infinite backwards, 5s position linear infinite backwards;
        }

        .pyro>.after {
            -moz-animation-delay: 1.25s, 1.25s, 1.25s;
            -webkit-animation-delay: 1.25s, 1.25s, 1.25s;
            -o-animation-delay: 1.25s, 1.25s, 1.25s;
            -ms-animation-delay: 1.25s, 1.25s, 1.25s;
            animation-delay: 1.25s, 1.25s, 1.25s;
            -moz-animation-duration: 1.25s, 1.25s, 6.25s;
            -webkit-animation-duration: 1.25s, 1.25s, 6.25s;
            -o-animation-duration: 1.25s, 1.25s, 6.25s;
            -ms-animation-duration: 1.25s, 1.25s, 6.25s;
            animation-duration: 1.25s, 1.25s, 6.25s;
        }

        @-webkit-keyframes bang {
            from {
                box-shadow: 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white;
            }
        }

        @-moz-keyframes bang {
            from {
                box-shadow: 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white;
            }
        }

        @-o-keyframes bang {
            from {
                box-shadow: 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white;
            }
        }

        @-ms-keyframes bang {
            from {
                box-shadow: 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white;
            }
        }

        @keyframes bang {
            from {
                box-shadow: 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white, 0 0 white;
            }
        }

        @-webkit-keyframes gravity {
            to {
                transform: translateY(200px);
                -moz-transform: translateY(200px);
                -webkit-transform: translateY(200px);
                -o-transform: translateY(200px);
                -ms-transform: translateY(200px);
                opacity: 0;
            }
        }

        @-moz-keyframes gravity {
            to {
                transform: translateY(200px);
                -moz-transform: translateY(200px);
                -webkit-transform: translateY(200px);
                -o-transform: translateY(200px);
                -ms-transform: translateY(200px);
                opacity: 0;
            }
        }

        @-o-keyframes gravity {
            to {
                transform: translateY(200px);
                -moz-transform: translateY(200px);
                -webkit-transform: translateY(200px);
                -o-transform: translateY(200px);
                -ms-transform: translateY(200px);
                opacity: 0;
            }
        }

        @-ms-keyframes gravity {
            to {
                transform: translateY(200px);
                -moz-transform: translateY(200px);
                -webkit-transform: translateY(200px);
                -o-transform: translateY(200px);
                -ms-transform: translateY(200px);
                opacity: 0;
            }
        }

        @keyframes gravity {
            to {
                transform: translateY(200px);
                -moz-transform: translateY(200px);
                -webkit-transform: translateY(200px);
                -o-transform: translateY(200px);
                -ms-transform: translateY(200px);
                opacity: 0;
            }
        }

        @-webkit-keyframes position {

            0%,
            19.9% {
                margin-top: 10%;
                margin-left: 40%;
            }

            20%,
            39.9% {
                margin-top: 40%;
                margin-left: 30%;
            }

            40%,
            59.9% {
                margin-top: 20%;
                margin-left: 70%;
            }

            60%,
            79.9% {
                margin-top: 30%;
                margin-left: 20%;
            }

            80%,
            99.9% {
                margin-top: 30%;
                margin-left: 80%;
            }
        }

        @-moz-keyframes position {

            0%,
            19.9% {
                margin-top: 10%;
                margin-left: 40%;
            }

            20%,
            39.9% {
                margin-top: 40%;
                margin-left: 30%;
            }

            40%,
            59.9% {
                margin-top: 20%;
                margin-left: 70%;
            }

            60%,
            79.9% {
                margin-top: 30%;
                margin-left: 20%;
            }

            80%,
            99.9% {
                margin-top: 30%;
                margin-left: 80%;
            }
        }

        @-o-keyframes position {

            0%,
            19.9% {
                margin-top: 10%;
                margin-left: 40%;
            }

            20%,
            39.9% {
                margin-top: 40%;
                margin-left: 30%;
            }

            40%,
            59.9% {
                margin-top: 20%;
                margin-left: 70%;
            }

            60%,
            79.9% {
                margin-top: 30%;
                margin-left: 20%;
            }

            80%,
            99.9% {
                margin-top: 30%;
                margin-left: 80%;
            }
        }

        @-ms-keyframes position {

            0%,
            19.9% {
                margin-top: 10%;
                margin-left: 40%;
            }

            20%,
            39.9% {
                margin-top: 40%;
                margin-left: 30%;
            }

            40%,
            59.9% {
                margin-top: 20%;
                margin-left: 70%;
            }

            60%,
            79.9% {
                margin-top: 30%;
                margin-left: 20%;
            }

            80%,
            99.9% {
                margin-top: 30%;
                margin-left: 80%;
            }
        }

        @keyframes position {

            0%,
            19.9% {
                margin-top: 10%;
                margin-left: 40%;
            }

            20%,
            39.9% {
                margin-top: 40%;
                margin-left: 30%;
            }

            40%,
            59.9% {
                margin-top: 20%;
                margin-left: 70%;
            }

            60%,
            79.9% {
                margin-top: 30%;
                margin-left: 20%;
            }

            80%,
            99.9% {
                margin-top: 30%;
                margin-left: 80%;
            }
        }

    </style>
@endsection
@section('content')


    <div id="box-survey-hide">
        <div id="close"><i class="fa fa-arrow-down" aria-hidden="true"></i></div>
        <form action="" id="myForm" method="POST">
            <input type="hidden" name="survey_id" value="{{ $survey->id }}">
            @foreach ($questions as $question)
                <fieldset id="{{ $question->id }}">
                    @switch($question->type)
                        {{-- Text --}}
                        @case($question->type == 'text')
                            <div class="form-group">
                                <label class="control-label">{{ $question->name }}</label>
                                <div class="hr-line-dashed"></div>
                                <div>
                                    <input type="text" class="form-control" name="{{ $question->id }}" style="color: black"
                                        data-target="text" placeholder="Eg:abc...">
                                </div>
                            </div>
                        @break

                        {{-- Email --}}
                        @case($question->type == 'email')
                            <div class="form-group">
                                <label class="control-label">{{ $question->name }}</label>
                                <div class="hr-line-dashed"></div>
                                <div class="">
                                    <input type="email" class="form-control" name="{{ $question->id }}" style="color: black"
                                        data-target="email" placeholder="Eg:abc@example.com...">
                                </div>
                            </div>
                        @break

                        {{-- Radio --}}
                        @case($question->type == 'radio_button')
                            <div class="form-group">
                                <label class="control-label">
                                    {{ $question->name }}
                                </label>
                                <div class="hr-line-dashed"></div>
                                <div class="">
                                    <input type="hidden" value="radio" name="{{ $question->id . '[type]' }}"> <i></i>
                                    @foreach ($question->answers->where('status', 1) as $answer)
                                        <div class="radio_ans">
                                            <label class="i-checks">
                                                <input type="radio" value="{{ $answer->id }}"
                                                    name="{{ $question->id . '[value]' }}" data-target="radio"> <i></i>
                                                {{ $answer->value }}
                                                @if ($answer->has_comment == 1)
                                                <textarea class="comment" name="{{ $question->id . '[comment]' . '[]' }}"
                                                    cols="30" rows="3" style="color: black"></textarea>
                                                @else
                                                <textarea class="comment_null" name="{{ $question->id . '[comment]' . '[]' }}"
                                                    cols="30" rows="3" style="color: black; display:none"></textarea>
                                                @endif
                                            </label>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        @break

                        {{-- Checkbox --}}
                        @case($question->type == 'checkbox')
                            <div class="form-group">
                                <label class="control-label">
                                    {{ $question->name }}
                                </label>
                                <div class="hr-line-dashed"></div>

                                <div style="text-align: left">
                                    <input type="hidden" value="checkbox" name="{{ $question->id . '[type]' }}"> <i></i>
                                    @foreach ($question->answers->where('status', 1) as $answer)
                                        <div class="radio_ans">
                                            <label class="i-checks">
                                                <input type="checkbox" value="{{ $answer->id . '**' . $answer->value }}"
                                                    name="{{ $question->id . '[value]' . '[]' }}" data-target="checkbox" />
                                                {{ $answer->value }}
                                            </label>
                                        </div>
                                    @endforeach

                                </div>
                            </div>
                        @break

                        {{-- Rating --}}
                        @case($question->type == 'rating')
                            <div class="form-group">
                                <label class="control-label">{{ $question->name }}</label>
                                <div class="hr-line-dashed"></div>
                                <div class="">
                                    <span class="rating-star" style="">
                                        <input type="radio" name="{{ $question->id }}" value="5_star"
                                            data-target="rating"><span class="star"></span>

                                        <input type="radio" name="{{ $question->id }}" value="4_star"
                                            data-target="rating"><span class="star"></span>

                                        <input type="radio" name="{{ $question->id }}" value="3_star"
                                            data-target="rating"><span class="star"></span>

                                        <input type="radio" name="{{ $question->id }}" value="2_star"
                                            data-target="rating"><span class="star"></span>

                                        <input type="radio" name="{{ $question->id }}" value="1_star"
                                            data-target="rating"><span class="star"></span>
                                    </span>
                                </div>
                            </div>
                        @break
                        @default
                    @endswitch

                    <div class="error"></div>
                    <div class="hr-line-dashed"></div>
                    <div style="margin-top: 25px">
                        <button class="btn btn-grey pre" type="button" style="float:left; color:black">Previous</button>
                        <button class="btn btn-primary next" type="button" style="float:right">Next</button>
                        <button class="btn btn-primary finish" type="button" id="finish-survey"
                            style="float:right; display:none">Finish</button>
                        <div style="clear: both"></div>
                    </div>

                </fieldset>

            @endforeach


            <div id="thank">
                
                <div class="pyro">
                    <div class="before"></div>
                    <p style="color: aqua; font-family: cursive;">Thank You</p>
                    <div class="after"></div>
                </div>
            </div>

        </form>
    </div>

    <div id="box-survey-show">
        <div id="show"><i class="fa fa-arrow-up" aria-hidden="true"></i></div>
    </div>
@endsection

@section('custom_js')

<script>
    //Email validation
    function validateEmail(email) {
        const re =
            /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    //Display text error
    function error_text(err) {
        $(".error").text(err);
        $(".error").animate({
            opacity: '0.5'
        }, "fast");
        $(".error").animate({
            opacity: '1'
        }, "fast");
        count_error++;
    }

    //Close survey
    $("#close").click(function() {
        $("#box-survey-hide").animate({
            bottom: "-100%",
            opacity: "0",
        }, 2000);
        $("#box-survey-show").animate({
            bottom: "0",
            opacity: "1",
        }, 1000);
    });

    //Display survey
    $("#show").click(function() {
        $("#box-survey-show").animate({
            bottom: "-100%",
            opacity: "0",
        });
        $("#box-survey-hide").animate({
            bottom: "0",
            opacity: "1",
        }, 1000);
    });

    var current_qs, next_qs, pre_qs;
    var count_error = 0;


    //Display comment at radio button
    $("input[data-target=radio]").click(function() {
        var cmt_not_check = $("input[data-target=radio]:not(:checked)").siblings("textarea");
        cmt_not_check.val('');
        cmt_not_check.removeClass("active");
        var cmt = $("input[data-target=radio]:checked").siblings("textarea");
        cmt.addClass("active");
    });




    //Active for fisrt fieldset
    $("#{{ $questions->first()->id }}").addClass("active");
    $("#{{ $questions->first()->id }}").find(".pre").hide();
    $("#{{ $questions->first()->id }}").find(".next").removeAttr('style');
    $("#{{ $questions->first()->id }}").find("div").has(".next").css({
        "text-align": "center"
    });


    //Active for fieldset
    //Next fieldset
    
    $(".next").click(function() {
        $(this).closest("fieldset").find(".comment_null").val(" ");
        var text_length = $.trim($(this).closest("fieldset").find("input[data-target=text]").val());
        var email_length = $.trim($(this).closest("fieldset").find("input[data-target=email]").val());
        var radio_val = $(this).closest("fieldset").find("input[data-target=radio]:checked").val();
        var checkbox_val = $(this).closest("fieldset").find("input[data-target=checkbox]:checked").val();
        var rating_val = $(this).closest("fieldset").find("input[data-target=rating]:checked").val();
        var cmt = $(this).closest("fieldset").find("input[data-target=radio]:checked").siblings("textarea").val();


        if (text_length.length <= 0 && email_length.length <= 0 && !radio_val && !checkbox_val && !rating_val) {
            error_text("Please answer the question");
        } else if (text_length.length <= 0 && !validateEmail(email_length) && !radio_val && !checkbox_val && !
            rating_val) {
            error_text("Email is not valid");
        } else if (text_length.length <= 0 && email_length.length <= 0 && !checkbox_val && !rating_val && !cmt) {
            error_text("Please give your comment");
        } else {
            $(".error").text('');
            current_qs = $(this).closest("fieldset");
            current_qs.removeClass("active");
            next_qs = $(this).closest("fieldset").next();
            next_qs.addClass("active");
            count_error = 0;
        }
    });

    //Previous fieldset
    $(".pre").click(function() {
        current_qs = $(this).closest("fieldset");
        current_qs.removeClass("active");
        pre_qs = $(this).closest("fieldset").prev()
        pre_qs.addClass("active");
        pre_qs.find(".error").text('');
    });

    //Finish for last fieldset
    $("#{{ $questions->last()->id }}").find(".next").hide();
    $("#{{ $questions->last()->id }}").find(".finish").css({
        "display": "block"
    });

    //POST form by ajax
    $(document).on("click", "#finish-survey", function() {
        $(".next").click();
        if (count_error == 0) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ route('result') }}',
                type: 'POST',
                dataType: 'json',
                data: $("#myForm").serialize(),

                success: function(data) {
                    if (data == 1) {
                        console.log('oke');
                        $(".finish").closest("fieldset").removeClass("active");
                        $("#thank").css({
                            "display": "flex"
                        });
                        $("#box-survey-show").animate({
                            bottom: "-100%",
                            opacity: "0",
                        });
                        $("#box-survey-hide").animate({
                            bottom: "-100%",
                            opacity: "0",
                        }, 5000);

                    }

                },

                error: function() {
                    console.log('error');
                }
            });
        } else {
            return false;
        }

    });
</script>
@endsection
