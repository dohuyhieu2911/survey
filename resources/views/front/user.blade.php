@extends('layouts.app-front')
@section('content')
<div class="ibox-title">
    <h5>User Surveys <small>With custom checbox and radion elements.</small></h5>
    
</div>


<div class="ibox-content">
    @foreach ($surveys as $survey)
        <p>
            <a href='{{ url("user/survey/$survey->id") }}'>{{ $survey->name }}</a>
        </p>
    @endforeach
   
</div>  
@endsection