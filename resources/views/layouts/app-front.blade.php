<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token('') }}" />
    <title>User</title>

    <link href="{{ asset("css/bootstrap.min.css") }} " rel="stylesheet">
    <link href="{{ asset("font-awesome/css/font-awesome.css") }}" rel="stylesheet">
    <link href="{{ asset("css/plugins/iCheck/custom.css") }}" rel="stylesheet">
    <link href="{{ asset("css/animate.css") }}" rel="stylesheet">
    <link href="{{ asset("css/style.css") }}" rel="stylesheet">

    <link href="{{ asset("css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css") }}" rel="stylesheet">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    @yield('custom_css')
</head>

<body style="background-color: white">
    <div>   
        @yield('content')    
    </div>

    <!-- Mainly scripts -->
    <script src="{{ asset("js/jquery-3.1.1.min.js") }}"></script>
    <script src="{{ asset("js/bootstrap.min.js") }}"></script>
    <!-- Custom and plugin javascript -->
    <script src="{{ asset("js/plugins/pace/pace.min.js") }}"></script>
    @yield('custom_js')
</body>

</html>
