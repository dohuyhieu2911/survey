@extends('layouts.app-back')

@section('content')
<div class="ibox">
    <div class="ibox-title">
        <h5>Questions Create</h5>
        <div class="ibox-tools">
            <a class="collapse-link">
                <i class="fa fa-chevron-up"></i>
            </a>
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <i class="fa fa-wrench"></i>
            </a>
            <ul class="dropdown-menu dropdown-user">
                <li><a href="#" class="dropdown-item">Config option 1</a>
                </li>
                <li><a href="#" class="dropdown-item">Config option 2</a>
                </li>
            </ul>
            <a class="close-link">
                <i class="fa fa-times"></i>
            </a>
        </div>
    </div>

    <div class="ibox-content">
        <form  method="POST" action="{{ url('questions') }}">
            {{ csrf_field() }}
            <div class="form-group row">
                <input type="hidden" class="form-control" id="" name="survey_id" value="{{ $survey->id }}">
            </div>
    
            <div class="form-group row">
                <label class="col-lg-2 col-form-label">Name</label>
                <div class="col-lg-10">
                    <input type="text" class="form-control" id="" name="name">
                </div>
            </div>
    
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Type</label>
                <div class="col-sm-10">
                    <div class="form-check">
                        <input class="form-check-input i-checks" type="radio" name="type" id="exampleRadios1" value="text" checked>
                        <label class="form-check-label" for="exampleRadios1">
                           Text
                        </label>
                      </div>
                    <div class="form-check">
                        <input class="form-check-input i-checks" type="radio" name="type" id="exampleRadios2" value="email">
                        <label class="form-check-label" for="exampleRadios2">
                            Email
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input i-checks" type="radio" name="type" id="exampleRadios3" value="radio_button">
                        <label class="form-check-label" for="exampleRadios3">
                            Radio_button
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input i-checks" type="radio" name="type" id="exampleRadios4" value="checkbox">
                        <label class="form-check-label" for="exampleRadios4">
                            Checkbox
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input i-checks" type="radio" name="type" id="exampleRadios5" value="rating">
                        <label class="form-check-label" for="exampleRadios5">
                            Rating
                        </label>
                    </div>
                </div>
            </div>
    
            <div class="form-group row">
                <label class="col-lg-2 col-form-label">Sort</label>
                <div class="col-lg-10">
                    <input type="text" class="form-control" id="" name="sort">
                </div>
            </div>
        
            <div class="hr-line-dashed"></div>
            <div class="form-group row">
                <a href="{{ url("question/$survey->id") }}" class="btn btn-white">Cancel</a>
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>

        </form>
    </div>
</div>
    
@endsection