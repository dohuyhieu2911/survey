@extends('layouts.app-back')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Question Tables {{ $survey->name }}</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ url('/admin') }}">Home</a>
                </li>
                <li>
                    <a>Tables</a>
                </li>
                <li>
                    <a href="{{ url('/surveys') }}">Surveys Tables</a>
                </li>
                <li class="active">
                    <strong>{{ $survey->name }}</strong>
                </li>
            </ol>
        </div>

        <div class="col-lg-2">
            <a class="btn btn-primary h3 mb-0 text-gray-800" href="{{ url("questions/create/$survey->id") }}">+ Add
                question</a>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Question Tables example with responsive plugin</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li><a href="#">Config option 1</a>
                                </li>
                                <li><a href="#">Config option 2</a>
                                </li>
                            </ul>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-example dataTable"
                                id="ques_table" aria-describedby="DataTables_Table_0_info" role="grid">
                                @if ($questions->isNotEmpty())
                                    <thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Survey_id</th>
                                            <th scope="col">Name</th>
                                            <th scope="col">Type</th>
                                            <th scope="col">Status</th>
                                            <th scope="col">Sort</th>
                                            <th scope="col">Created-at</th>
                                            <th scope="col">Updated-at</th>
                                            <th scope="col"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($questions as $question)
                                            <tr>
                                                <th scope="col">{{ $question->id }}</th>
                                                <td>{{ $question->survey_id }}</td>
                                                <td>{{ $question->name }}</td>
                                                <td>{{ $question->type }}</td>
                                                <td>{{ $question->status }}</td>
                                                <td>{{ $question->sort }}</td>
                                                <td>{{ $question->created_at }}</td>
                                                <td>{{ $question->updated_at }}</td>
                                                <td>
                                                    @if ($question->type == 'radio_button' || $question->type == 'checkbox')
                                                        <a class="btn btn-primary" style="margin-right: 8px"
                                                            href='{{ url("/answers/create/$question->id") }}'>Modify Answer
                                                        </a>
                                                    @endif
                                                    <a class="btn btn-warning" style="margin-right: 8px"
                                                        href='{{ url("/questions/$question->id") }}'>Edit</a>
                                                    <a class="btn btn-danger" href="javascript:void(0)"
                                                        onclick="return confirm('Are you sure delete this question?') ? document.getElementById('question-delete-{{ $question->id }}').submit() : false">Delete</a>
                                                    <form action='{{ url("/questions/$question->id") }}' method="POST"
                                                        id="question-delete-{{ $question->id }}">
                                                        {{ method_field('DELETE') }}
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="survey_id" value="{{ $survey->id }}">
                                                    </form>

                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                @else
                                    <div>
                                        <div class="alert alert-warning" role="alert">
                                            No questions Found!
                                        </div>
                                    </div>
                                @endif
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom_js')
    <script>
        $(document).ready(function() {
            $('#ques_table').DataTable();
        });
    </script>
@endsection
