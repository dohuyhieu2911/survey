@extends('layouts.app-back')

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Results Tables</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/admin') }}">Home</a>
            </li>
            <li>
                <a>Tables</a>
            </li>
            <li class="active">
                <strong>Results Tables</strong>
            </li>
        </ol>
    </div>
   
</div>
       
    @if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
    @endif

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Results Tables </h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li><a href="#">Config option 1</a>
                                </li>
                                <li><a href="#">Config option 2</a>
                                </li>
                            </ul>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">

                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-example" id="result_table" aria-describedby="DataTables_Table_0_info" role="grid">
                                @if($results->isNotEmpty())
                                <thead>
                                    <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Survey_id</th>
                                    <th scope="col">Question_id</th>
                                    <th scope="col">Answer_id</th>
                                    <th scope="col">Comment</th>
                                    <th scope="col">Created-at</th>
                                    <th scope="col">Updated-at</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach ($results as $result)
                                    <tr>
                                        <th scope="col">{{ $result->id }}</th>
                                        <td>{{ $result->survey_id }}</td>
                                        <td>{{ $result->question_id }}</td>
                                        <td>{{ $result->answer_id }}</td>
                                        <td>{{ $result->comment }}</td>
                                        <td>{{ $result->created_at }}</td>
                                        <td>{{ $result->updated_at }}</td>
                                       
                                    </tr>
                                @endforeach
                                </tbody>
                                @else
                                <div>
                                    <div class="alert alert-warning" role="alert">
                                        No Results Found!
                                    </div>
                                </div>
                            @endif
                                
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom_js')
    <script>
        $(document).ready(function() {
            $('#result_table').DataTable();
        });
    </script>
@endsection

