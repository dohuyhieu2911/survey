@extends('layouts.app-back')

@section('content')
    <div class="ibox"> 
        <div class="ibox-title">
            <h5>Surveys Create</h5>
            <div class="ibox-tools">
                <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                </a>
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-wrench"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li><a href="#" class="dropdown-item">Config option 1</a>
                    </li>
                    <li><a href="#" class="dropdown-item">Config option 2</a>
                    </li>
                </ul>
                <a class="close-link">
                    <i class="fa fa-times"></i>
                </a>
            </div>
        </div>
        <div class="ibox-content">
            <form  method="POST" action="{{ url('surveys') }}">
                {{ csrf_field() }}
                <div class="form-group row">
                    <label class="col-lg-2 col-form-label">Name</label>
                    <div class="col-lg-10">
                        <input type="text" class="form-control" id="" name="name">
                    </div>                   
                </div>
        
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Status</label>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <input class="form-check-input i-checks" type="radio" name="status" id="exampleRadios1" value="1" checked>
                            <label class="form-check-label" for="exampleRadios1">
                              Yes 
                            </label>
                        </div>
            
                        <div class="form-check">
                            <input class="form-check-input i-checks" type="radio" name="status" id="exampleRadios2" value="0">
                            <label class="form-check-label" for="exampleRadios1">
                                No 
                              </label>
                        </div>
                    </div>  
                </div>
         
                <div class="hr-line-dashed"></div>
                <div class="form-group row">
                    <a href="{{ url('surveys') }}" class="btn btn-white">Cancel</a>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>

            </form>
        </div>
    </div>
    
    
@endsection