@extends('layouts.app-back')

@section('content')

    <div class="ibox">
        <div class="ibox-title">
            <h5>Survey {{ $survey->name }} Edit</h5>
            <div class="ibox-tools">
                <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                </a>
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-wrench"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li><a href="#" class="dropdown-item">Config option 1</a>
                    </li>
                    <li><a href="#" class="dropdown-item">Config option 2</a>
                    </li>
                </ul>
                <a class="close-link">
                    <i class="fa fa-times"></i>
                </a>
            </div>
        </div>

        <div class="ibox-content">
            <form method="POST" action='{{ url("surveys/$survey->id") }}'>
                {{ method_field('PUT') }}
                {{ csrf_field() }}
                <div class="form-group row">
                    <label class="col-lg-2 col-form-label">Name</label>
                    <div class="col-lg-10">
                        <input type="text" name="name" id="" class="form-control" placeholder="" aria-describedby="helpId" value="{{ $survey->name }}">
                    </div>       
                </div>
        
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Status</label>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <input class="form-check-input i-checks" type="radio" name="status" id="exampleRadios1" value="1" 
                            @if ($survey->status == 1)
                                checked
                            @endif>
                            <label class="form-check-label" for="exampleRadios1">
                              Yes 
                            </label>
                          </div>
                        <div class="form-check">
                            <input class="form-check-input i-checks" type="radio" name="status" id="exampleRadios2" value="0"
                            @if ($survey->status == 0)
                                checked
                            @endif>
                            <label class="form-check-label" for="exampleRadios2">
                              No
                            </label>
                        </div>
                    </div>
                </div>

                <div class="hr-line-dashed"></div>
        
                <div class="form-group row">
                    <a href="{{ url('surveys') }}" class="btn btn-white">Cancel</a>
                    <button class="btn btn-primary" type="submit">Submit</button>
                </div>
            </form>
        </div>
    </div>

    
@endsection
