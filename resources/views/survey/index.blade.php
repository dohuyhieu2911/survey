@extends('layouts.app-back')

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Surveys Tables</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/admin') }}">Home</a>
            </li>
            <li>
                <a>Tables</a>
            </li>
            <li class="active">
                <strong>Surveys Tables</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
        <a class="btn btn-primary h3 mb-0 text-gray-800" href="{{ url('surveys/create') }}">+ Add Survey</a>
    </div>
</div>
       
    @if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
    @endif

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Surveys Tables </h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li><a href="#">Config option 1</a>
                                </li>
                                <li><a href="#">Config option 2</a>
                                </li>
                            </ul>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">

                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-example" id="survey_table" aria-describedby="DataTables_Table_0_info" role="grid">
                                @if($surveys->isNotEmpty())
                                <thead>
                                    <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Created-at</th>
                                    <th scope="col">Updated-at</th>
                                    <th scope="col"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach ($surveys as $survey)
                                    <tr>
                                        <th scope="col">{{ $survey->id }}</th>
                                        <td>{{ $survey->name }}</td>
                                        <td>{{ $survey->status }}</td>
                                        <td>{{ $survey->created_at }}</td>
                                        <td>{{ $survey->updated_at }}</td>
                                        <td>
                                            <a class="btn btn-primary" href="{{ url("/question/$survey->id") }}">See All Question</a>
                                            <a class="btn btn-warning" style="margin-right: 8px" href='{{ url("/surveys/$survey->id") }}'>Edit</a>
                                            <a class="btn btn-danger" href="javascript:void(0)" onclick="return confirm('Are you sure delete this survey?') ? document.getElementById('survey-delete-{{ $survey->id }}').submit() : false">Delete</a>
                                            <form action='{{ url("/surveys/$survey->id") }}' method="POST" id="survey-delete-{{ $survey->id }}">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                            </form>
                            
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                @else
                                <div>
                                    <div class="alert alert-warning" role="alert">
                                        No Surveys Found!
                                    </div>
                                </div>
                            @endif
                                
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom_js')
    <script>
        $(document).ready(function() {
            $('#survey_table').DataTable();
        });
    </script>
@endsection

