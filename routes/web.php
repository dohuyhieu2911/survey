<?php


use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function() {
    return view('welcome');
});

Route::get('/admin', function () {
    return view('layouts.dashboard');
});



Route::get('/surveys', 'SurveyController@index');
Route::get('/surveys/create', 'SurveyController@create');
Route::post('/surveys', 'SurveyController@store');
Route::get('/surveys/{id}', 'SurveyController@edit');
Route::put('/surveys/{id}', 'SurveyController@update');

Route::delete('/surveys/{id}', 'SurveyController@destroy');

Route::get('/question/{id}', 'QuestionController@index');
Route::get('/questions/create/{id}', 'QuestionController@create');
Route::post('/questions', 'QuestionController@store');
Route::get('/questions/{id}', 'QuestionController@edit');
Route::put('/questions/{id}', 'QuestionController@update');
Route::delete('/questions/{id}', 'QuestionController@destroy');


Route::get('/answers/create/{id}', 'AnswerController@create');
Route::post('/answers/create', 'AnswerController@store')->name('answers.create');
Route::get('/answers/{id}', 'AnswerController@edit');
Route::put('/answers/{id}', 'AnswerController@update');
Route::delete('/answers/{id}', 'AnswerController@destroy');

Route::get('/user', 'UserController@index')->name('user');
Route::get('/user/survey/{id?}', 'UserController@chooseSurvey')->name('user.survey');

Route::get('/result', 'ResultController@index');
Route::post('/result', 'ResultController@store')->name('result');





